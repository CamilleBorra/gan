<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit9fae703646fd44c6028d6e2a0027b0e2
{
    public static $prefixLengthsPsr4 = array (
        'S' => 
        array (
            'Symfony\\Component\\EventDispatcher\\' => 34,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Symfony\\Component\\EventDispatcher\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/event-dispatcher',
        ),
    );

    public static $prefixesPsr0 = array (
        'G' => 
        array (
            'Guzzle\\Tests' => 
            array (
                0 => __DIR__ . '/..' . '/guzzle/guzzle/tests',
            ),
            'Guzzle' => 
            array (
                0 => __DIR__ . '/..' . '/guzzle/guzzle/src',
            ),
        ),
        'A' => 
        array (
            'Aws' => 
            array (
                0 => __DIR__ . '/..' . '/aws/aws-sdk-php/src',
            ),
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit9fae703646fd44c6028d6e2a0027b0e2::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit9fae703646fd44c6028d6e2a0027b0e2::$prefixDirsPsr4;
            $loader->prefixesPsr0 = ComposerStaticInit9fae703646fd44c6028d6e2a0027b0e2::$prefixesPsr0;
            $loader->classMap = ComposerStaticInit9fae703646fd44c6028d6e2a0027b0e2::$classMap;

        }, null, ClassLoader::class);
    }
}
