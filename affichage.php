<?php 
    require_once "bdd.php";
    require 'vendor/autoload.php';
    require_once 'passwordBG.php';
	
	use Aws\S3\S3Client;
	use Aws\S3\Exception\S3Exception;

	// AWS Info
	

	// Connect to AWS
	try {
		// You may need to change the region. It will say in the URL when the bucket is open
		// and on creation.
		$s3 = S3Client::factory(
			array(
				'credentials' => array(
					'key' => $key ,
					'secret' => $secret,
				),
				'version' => 'latest',
				'region'  => $region
			)
		);
       
	} catch (Exception $e) {
		// We use a die, so if this fails. It stops here. Typically this is a REST call so this would
		// return a json object.
		die("Error: " . $e->getMessage());
	}
   //https://cams5.s3.eu-west-1.amazonaws.com/cams5/GAN2101_A07407_AFF_ABRIBUS_3B3_1185x1750_V1_298.png
    
    //echo $result['Body'];
    
    if(isset($_GET["ville"])&& !empty($_GET["ville"])){
        $ville=$_GET["ville"];
        $img=$db->query("SELECT urlPhoto,nomDuVisuel,mob,type FROM gan WHERE ville=\"$ville\" AND urlPhoto!=\"NULL\"");
    }
    $cities=$db->query("SELECT COUNT(urlPhoto) as nb, ville FROM gan GROUP BY ville ");

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="style/style.css" type="text/css">
    <title>Document</title>
</head>
<body>
  
    <select name="ville" id="ville">
        <?php if(isset($_GET["ville"])&& !empty($_GET["ville"])):?>
            <option value="<?=$_GET['ville']?>"><?= $_GET['ville']?></option>
        <?php else: ?>
            <option value="default"></option>
        <?php endif;?>
        
        <?php while($citiesExe=$cities->fetch()):?>
        <option value="<?=$citiesExe['ville']?>"><?= $citiesExe['ville']." (".$citiesExe['nb'].")"?></option>
        <?php endwhile;?>
    </select>

    <?php if(isset($_GET["ville"])&& !empty($_GET["ville"])):?>
    
        <div class="allImages">

        <?php while($imgExe=$img->fetch()):?>
            
    
                <figure>
                    <a  class=" lien" href=<?= "https://cams6.s3.eu-west-1.amazonaws.com/PTDR/".$imgExe['urlPhoto']?>>
                    <img class="photo" id=<?= $imgExe['urlPhoto']?> src=<?= "https://cams6.s3.eu-west-1.amazonaws.com/PTDR/".$imgExe['urlPhoto']?> alt=""></a>
                    <figcaption><?= $imgExe['type'].'-'.$imgExe['mob']?></figcaption>
                </figure>
            
        <?php endwhile?>
        </div>
    <?php endif?>
</body>
</html>

<script src="style/jquery.js"></script>
<script>
$(document).ready(function () {
        $('#ville').change(function () {


            window.location.href = `affichage.php?ville=${document.getElementById('ville').value}`
        })

   
       
    })

</script>